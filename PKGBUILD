# Maintainer: Márcio Sousa Rocha <marciosr10@gmail.com>
# Baseado no PKGBUILD feito por Lara Maia <lara@craft.net.br>
 
_date=2021
pkgname=irpf
java=11
_pkgver=1.9
pkgver=${_date}.${_pkgver}
pkgrel=1
license=('custom')
arch=(any)
pkgdesc='Programa Oficial da Receita para elaboração do IRPF'
url='https://www.receita.fazenda.gov.br'
makedepends=('imagemagick')
install=${pkgname}.install
source=(https://downloadirpf.receita.fazenda.gov.br/irpf/${_date}/irpf/arquivos/IRPF${pkgver/./-}.zip
        "https://metainfo.manjariando.com.br/${pkgname}/gov.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/receita-federal.png"
        "${pkgname}"
        "Copyright")
sha256sums=('1d0cdbb00e141caf2d65b85be88b560b86b3633bf117b7d34221659b89c8e5ee'
            '6ba9e47e9e9f02ced724bf176fd106eecf0f9baf8ffe49c21a719c2d5dad2ae2'
            '541a33893db3730e4d858d5656de055b957801ad958f827b6cd30f308f663684'
            '069b8533097fa2a2293ecf27582d63fbb1b44acaf1ada24baf35e698cfecc645'
            'abd34d009d96ec93b802f4215395bb317349867a3f4420bf398fc8ba8d9a71a7')

_irpf_desktop="[Desktop Entry]
Name=Imposto de Renda Pessoa Física
Comment=Programa Oficial da Receita para elaboração do IRPF
Exec=irpf
StartupNotify=true
Icon=irpf
Terminal=false
Type=Application
Categories=Network;"

build() {
    cd "${srcdir}"
    echo -e "$_irpf_desktop" | tee gov.${pkgname}.desktop
}
 
package() {
    depends=("java-runtime=${java}" 'hicolor-icon-theme' 'sh')

    cd "${srcdir}"/IRPF${_date}
    mkdir -p "${pkgdir}"/{usr/bin,usr/share/irpf}
       
    cp -rf lib "$pkgdir"/usr/share/irpf/
    cp -rf lib-modulos "$pkgdir"/usr/share/irpf/
    cp -rf help "$pkgdir"/usr/share/irpf/
      
    install -Dm755 irpf.jar "$pkgdir"/usr/share/irpf/

    install -Dm644 Leia-me.htm "$pkgdir"/usr/share/irpf/
    install -Dm644 offline.png "$pkgdir"/usr/share/irpf/
    install -Dm644 online.png "$pkgdir"/usr/share/irpf/
    install -Dm644 pgd-updater.jar "$pkgdir"/usr/share/irpf/

    install -Dm755 "$srcdir"/irpf "$pkgdir"/usr/bin/
       
    # Appstream
    install -Dm644 "${srcdir}/gov.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/gov.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/Copyright" "${pkgdir}/usr/share/licenses/${pkgname}/Copyright"
    install -Dm644 "${srcdir}/gov.${pkgname}.desktop" "${pkgdir}/usr/share/applications/gov.${pkgname}.desktop"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/receita-federal.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done

    sed -i "s:JAVA=.*:JAVA=${java}:" "${startdir}/${pkgname}.install"
}
